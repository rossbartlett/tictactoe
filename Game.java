import java.util.Scanner;

/*
improved takeTurn() using a try/catch
removed variablle turn
 improved choose-row
 improved choose-Col switch
 */

public class Game {
    //size of board is 3x3
    Tile [][] board = new Tile [3][3];
    Player p1 = new Player("Player 1", 'X');
    Player p2 = new Player("Player 2", 'O');
    Player current=p1;
    public Game() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j <3 ; j++) {
                board[i][j] = new Tile();
            }
            
        }
    }
    
    public void changePlayer(){
        if (current == p1) current = p2;
        else current = p1;
    }
    
    public void takeTurn(){
        do {
            Scanner reader = new Scanner(System.in);
            int choice=0;
            
            //get input
            while (true){
                System.out.println(current.name + ", where would you like to play?");
                String c = reader.next();
                try{
                    choice = Integer.parseInt(c);
                    if (choice >=1 && choice <=9) break; //break when success
                } catch (NumberFormatException e) {
                    // not an integer!
                }
                System.out.println("Invalid input: "+c);
            }
            
            //determine row
            int chosenRow = (choice-1)/3;
            
            //determine column
            int chosenCol = 0;
            switch (choice) {
                case 1:case 4:case 7:
                    chosenCol = 0;
                    break;
                case 2:case 5:case 8:
                    chosenCol = 1;
                    break;
                case 3:case 6:case 9:
                    chosenCol = 2;
                    break;
                default:
                    System.out.println("ERROR IN SWITCH determining col");
                    System.exit(1);
            }
            
            //make sure choosing an empty tile
            if (board[chosenRow][chosenCol].symbol==' ')
            {
                board[chosenRow][chosenCol].setTile(current);
                break;
            }
            else System.out.println("That square is taken already.");
            
        }while(true);
        
        // System.out.println(current.name +" placed an "+ board[chosenRow][chosenCol].symbol + " at ["+chosenRow+"]["+chosenCol+"]");
    }
    
    
    public void  displayBoard() {
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("[ " + board[i][j].symbol + " ] ");
            }
            System.out.println();
        }
    }
    
    public static void displayInitial(){
        for (int i = 1; i <= 9; i++) {
            System.out.print("[ " + i + " ] ");
            if (i%3==0) System.out.println();
        }
        System.out.println();
    }
    
    public boolean checkWin(){
        for (int i = 0; i < 3; i++) {
            //check row win
            if (board[i][0].symbol !=' ' && board[i][0].symbol == board[i][1].symbol && board[i][0].symbol == board[i][2].symbol )
            {
                System.out.println("Row win at row "+i);
                return true;
            }
            //check col win
            if (board[0][i].symbol !=' ' && board[0][i].symbol == board[1][i].symbol && board[0][i].symbol == board[2][i].symbol )
            {
                System.out.println("col win at col "+i);
                return true;
            }
        }
        //check diag \ win
        if (board[0][0].symbol !=' ' && board[0][0].symbol == board[1][1].symbol && board[0][0].symbol == board[2][2].symbol )
        {
            System.out.println("Diag win \\ ! ");
            return true;
        }
        //check diag / win
        if (board[2][0].symbol !=' ' && board[2][0].symbol == board[1][1].symbol && board[2][0].symbol == board[0][2].symbol )
        {
            System.out.println("Diag win / !");
            return true;
        }
        
        return false;
    }
    
    public static void main(String[] args) {
        Game game1 = new Game();
        
        
        System.out.println("\nNew game.");
        displayInitial();
        System.out.println("Player 1, X's, starts.");
        
        while(true){
            game1.displayBoard();
            game1.takeTurn();
            if(game1.checkWin()){
                game1.displayBoard();
                System.out.println(game1.current.name + " wins!");
                break;
            }
            game1.changePlayer();
        }
    }
}
